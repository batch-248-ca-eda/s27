What is a Data Model?

	A data model describes how data is organized and grouped in a database/

	By creating data models, we can anticipate which data will be managed by the database management systemn in accordance to the app to be developed.

Data Modelling

	Database should have a purpose and its organization must be related to the kind of application we are building

	// Scenario

		A course booking system application where a user can book into a course

	Type: Course Booking System
	Description: A course booking system application where a user can book into a course

	Features:
		- User Registration
		- User Authentication/Login

			Authenticated Users:
				View Course
				Enroll Course
				Update Details (with Admin verification)
				Delete Details (with Admin verification)

			Admin Users:
				Add Course
				Udpate Course
				Archive/De-activate Course
				Re-activate Course
				View All Courses (active/inactive)
				View and Manage User Accounts

			All Users(Guest, Authenticated, Admin):
				View Active Courses

	Data Models
		- Blueprints for our documents that we can follow and structure our data
		- Shows the relationship between our data

		user {

			id - unique identifier for the document,
			userName,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin

		}

		course {

			id - unique identifier for the document, 
			name,
			description,
			price,
			slots,
			schedule,
			instructor,
			isActive

		}

		transactions/enrollment {

			id - document identifier,
			userId - the unique identifier for the user,
			courseId - the unique identifier for the course,
			username - optional,
			courseName - optional,
			isPaid,
			dateEnrolled
		}


		Model Relationships

			To be able to properly organize an application database we should also be able to identify the relationship between our models

			One to One - this relationship means that a model is exclusively related to only one model

			Employee:

			{
				"id": "2023Dev",
				"firstName": "Cardo",
				"lastName": "Dalisay",
				"email": "jsdev2023@gmail.com"
			}

			Credentials:
			{
				"id": "creds_01",
				"employee_id": "2023Dev",
				"role": "developer",
				"team": "tech"
			}

		In MongoDB, one to one relationship can be expressed in another way instead of referencing.

		Embedding - embed/put another document in a document
			Subdocuments - are documents embedded in a parent document

		Employee
		{
			"id": "2023Dev",
			"firstName": "Cardo",
			"lastName": "Dalisay",
			"email": "jsdev2023@gmail.com"
			"credentials": {
				"id": "creds_01",
				"role": "developer",
				"team": "tech"
			}

		}

		One to Many

			One model is related to multiple other models
			However, the other models are only related to one

			Person - many email address

			email address - one person

			Blog post - comments

				A blog post can have multiple comments but each comment should only refer to a single blog post

			Blog: {

				"id": "blog-214",
				"title": "Happy Tuesday!",
				"content": "We are halfway through Febuary!",
				"createdOn": "02/14/2023",
				"author": "blogwriter1",
				"comments": [
					{
						"id": "blogcomment1",
						"comment": "Awesome blog!",
						"author": "blogwriter1"
					},
					{
						"id": "blogcomment2",
						"comment": "Booo. Your blog is not awesome!",
						"author": "notBasher23"
					}
				]
			}

			Comments: 
			{
				"id": "blogcomment1",
				"comment": "Awesome blog!",
				"author": "blogwriter1",
				"blog_id": "blog2-14"
			},
			{
				"id": "blogcomment2",
				"comment": "Booo. Your blog is not awesome!",
				"author": "notBasher23",
				"blog_id": "blog2-14"
			}


		Many to Many

		Multiple documents are related to multiple documents

		users - courses

		When a many to many relationship is created, for models to related to each other, an associative entity is created. Associative entity is a model that relates models in the many to many relationship

		user - enrollment/transactions - course 

		So that a user can relate to a course, so that we can track the enrollment of a user to a course, we have to create the details for their enrollment

		With Referencing:

		user {

			id - unique identifier for the document,
			userName,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin

		}

		course {

			id - unique identifier for the document, 
			name,
			description,
			price,
			slots,
			schedule,
			instructor,
			isActive

		}

		transactions/enrollment {

			id - document identifier,
			userId - the unique identifier for the user,
			courseId - the unique identifier for the course,
			username - optional,
			courseName - optional,
			isPaid,
			dateEnrolled
		}



		In MongoDB, many to many relationships can alsoe be expressed in other way:

		Two-way Embedding - in the two way embedding, the associative entity is created and embedded in both models/documents

		user {

			id - unique identifier for the document,
			userName,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments: [
			
				{	
				id - document identifier,
				courseId - the unique identifier for the course,
				courseName - optional,
				isPaid,
				dateEnrolled
				}

			]

		}

		course {

			id - unique identifier for the document, 
			name,
			description,
			price,
			slots,
			schedule,
			instructor,
			isActive,
			enrollees: [
				{
				id - document identifier,
				userId - the unique identifier for the user,
				username - optional,
				isPaid,
				dateEnrolled
				}
			]

		}
